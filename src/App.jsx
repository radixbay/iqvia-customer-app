import Styles from "./App.less";
import Studies from "./components/Studies.jsx";
import Pagination from "./components/Pagination.jsx";
import StudyFilter from "./components/StudyFilter.jsx";
import { defaultFilters } from "./variables";

import React from 'react';
export default class App extends React.Component {

  constructor(props) {
      super();
      this.ref = React.createRef();
      this.state = {
        studyList: [],
        resultsAvailable: true,
        currentPage: 1,
        studiesPerPage: 5,
        error: false,
        filters: defaultFilters,
        filteredStudies: [],
        filterOpts: {
          status: [],
          phase: []
        },
        showFilter: "none",
        sortOption: "Recently Published",
        searchTerm: quip.apps.getRootRecord().get("account").getTextContent(),
        dateFilter: "All"
      }
  }

  componentDidMount() {
    var searchTerm = this.state.searchTerm.trim();
    switch(searchTerm) {
      case 'Enter Account name...':
      case "[[Account.Name]]":
        this.setState({resultsAvailable: false});
        break;
      default:
        this.getClinicalStudies(searchTerm);
    }
  }

  /**
   * Retrieves the top 50 relevant clinical studies using the Clinical Trials API
   * @param {String} searchTerm The word or phrase used for retrieving search results
   */
  getClinicalStudies(searchTerm) {
    searchTerm = this.cleanSearchTerm(searchTerm);
    
    var apiURL = "https://clinicaltrials.gov/api/query/study_fields?expr=AREA[SponsorSearch]" + searchTerm + "&fields=NCTId%2CStudyFirstPostDate%2CCondition%2CBriefTitle%2CStartDate%2CPrimaryCompletionDate%2COverallStatus%2CPhase%2CLastUpdatePostDate&min_rnk=1&max_rnk=50&fmt=json";
    
    fetch(apiURL)
      .then(res => res.json())
      .then(
        (result) => {
          var studyList = [];
          var recentStudy = {};

          if(result.StudyFieldsResponse.NStudiesReturned == 0) {
            this.setState({
              resultsAvailable: false,
              filteredStudies: []
            });
          }
          else {
            this.setState({resultsAvailable: true});

            for (var study of result.StudyFieldsResponse.StudyFields) {
              recentStudy = {
                "name": study.BriefTitle,
                "url": "https://clinicaltrials.gov/show/" + study.NCTId,
                "published": study.StudyFirstPostDate,
                "condition": study.Condition.join(", "),
                "startDate": study.StartDate,
                "completionDate": study.PrimaryCompletionDate,
                "phase": study.Phase.join(", "),
                "phasePriority": this.setPhasePriority(study.Phase),
                "status": study.OverallStatus,
                "lastModified": study.LastUpdatePostDate
              }
              studyList = studyList.concat(recentStudy);
            }

            // Apply selected sort
            this.applySort(this.state.sortOption.trim(), studyList);
            this.setState({studyList: studyList});

            // Apply any selected filters
            studyList = this.applyDateFilter(this.state.dateFilter, studyList);
            studyList = this.applyCheckboxFilters(studyList, this.state.filterOpts);
            this.setState({filteredStudies: studyList});
          }
        },
        (error) => {
          console.log(error);
          this.setState({error: true}); // Displays error to user
        }
      )
  }

  /**
   * Takes the search term and removes invalid search parameters. Also encodes the search term.
   * @param {String} searchTerm The word or phrase used for retrieving search results
   * @return {String} cleanedTerm The cleaned search term
   */
  cleanSearchTerm(searchTerm) {
    var cleanedTerm = searchTerm.replace(/ *\[[^)]*\] */, "");
    cleanedTerm = encodeURIComponent(cleanedTerm);
    return cleanedTerm;
  }

  /**
   * Returns the phase priority for the Study, which is used for sorting.
   * @param {String} studyPhase The current phase of the study
   * @return {Number} phasePriorty The phase priority for the study
   */
  setPhasePriority(studyPhase) {
    var phasePriorty = 10;

    for(var filter of this.state.filters) {
      if(filter.type == "phase" && studyPhase.includes(filter.label)) {
        phasePriorty = filter.sortPriority;
      }
    }

    return phasePriorty;
  }

  /**
   * Determines the action(s) that should be performed when a checkbox filter is clicked
   * @param {Object} filter The filter that should be applied/removed to/from the list of studies
   */
  handleFilter = filter => {
    var filterSet;
    var filtered = [];

    if(filter.props.isChecked == false) {
      filterSet = this.setCheckedStatus(filter,false);
      filtered = this.setFilter(filter, true, filterSet);
    }
    else {
      filterSet = this.setCheckedStatus(filter,true);
      filtered = this.setFilter(filter, false, filterSet);
    }

    this.applySort(this.state.sortOption, filtered);

    this.setState({
      filteredStudies: filtered
    });

    if(filtered.length == 0) {
      this.setState({
        resultsAvailable: false
      });
    }
  }

  /**
   * Updates the checked status of all checkbox filters
   * @param {Object} filter The filter that should be applied/removed to/from the list of studies
   * @param {Boolean} checked Determines if a checkbox filter is checked or not
   * @return {Boolean} filterSet Determines if any filter is currently set
   */
  setCheckedStatus(filter, checked) {
    var filterSet = false;

    var updatedFilters = this.state.filters.map(fltr => {
      if(fltr.id == filter.props.id){
        if(checked) {
          fltr.isChecked = false;
        }
        else {
          fltr.isChecked = true;
          filterSet = true;
        }
      }

      return fltr;
    });
    
    this.setState({
      filters: updatedFilters
    });

    return filterSet;
  }

  /**
   * Sets the filters for the list of studies
   * @param {Object} filter The filter that should be applied/removed to/from the list of studies
   * @param {Boolean} addFilter Determines if a filter should be added or removed
   * @param {Boolean} filterSet Determines if any filter is currently set
   * @return {Map} filtered The updated list of studies with filters applied
   */
  setFilter = (filter, addFilter, filterSet) => {
    var filterList = this.state.filterOpts;

      this.setState({currentPage: 1});

      if(addFilter) {
        filterList[filter.props.type].push(filter.props.label);
      }
      else {
        const index = filterList[filter.props.type].indexOf(filter.props.label);
        if (index > -1) {
          filterList[filter.props.type].splice(index, 1);
        }
      }
      
      this.setState({filterOpts: filterList});
      
      var listToFilter = this.state.studyList;
      var filtered = this.applyCheckboxFilters(listToFilter, filterList);
      filtered = this.applyDateFilter(this.state.dateFilter, filtered);

      if(filtered.length == 0){
        if(filterSet) {
          this.setState({resultsAvailable: false});
        }
        else {
          // if no filters are set, return the default list of studies
          this.setState({resultsAvailable: true});
          filtered = listToFilter;
        }
      }
      else {
        this.setState({resultsAvailable: true});
      }

      return filtered;
  }

  /**
   * Applies checkbox filters to the list of studies
   * @param {Map} listToFilter The list of studies to filter 
   * @param {Map} filterList The list of checkbox filters to apply; filter categories = status or phase
   * @return {Map} filtered The updated list of studies with checkbox filters applied
   */
  applyCheckboxFilters(listToFilter, filterList) {
    var filtered = listToFilter.filter(function (study) {
      for (var filterCategory in filterList) {
        if (study[filterCategory] === undefined || (filterList[filterCategory].length > 0 && !(filterList[filterCategory].some(filter => study[filterCategory].includes(filter))))){
          return false;
        }
      }
      return true;
    });

    return filtered;
  }

  /**
   * Determines the action(s) that should be performed when a date filter is selected
   * @param {Object} event The onChange event that triggers the handleDateFilter logic
   */
  handleDateFilter = event => {
    var dateFilter = event.target.value;
    var filterList = this.state.filterOpts;
    var listToSort = this.state.studyList;

    var filtered = this.applyDateFilter(dateFilter, listToSort);
    filtered = this.applyCheckboxFilters(filtered, filterList);
    this.applySort(this.state.sortOption, filtered);

    this.setState({
      filteredStudies: filtered,
      currentPage: 1,
      dateFilter: dateFilter
    });
  }

  /**
   * Applies the desired date filter to the list of studies
   * @param {String} dateFilter The date filter that should be applied to the list of studies
   * @param {Map} listToSort The list of studies to filter
   * @return {Map} filtered The filtered list of studies
   */  
  applyDateFilter(dateFilter, listToSort){
    var numMonthsToPush = 0;

    switch(dateFilter) {
      case 'Next 3 Months':
        numMonthsToPush = 3;
        break;
      case 'Next 6 Months':
        numMonthsToPush = 6;
        break;
      case 'Next 12 Months':
        numMonthsToPush = 12;
        break;
      default:
        break;
    }

    var today = new Date();
    var futureDate = new Date();
    futureDate.setMonth(futureDate.getMonth() + numMonthsToPush);

    var filtered = listToSort.filter(function(study) {
      var studyDate = new Date(study.completionDate);
      if(dateFilter === 'All'){
        return study;
      }
      else if (studyDate >= today && studyDate <= futureDate){
        return study;
      }
    });

    return filtered;
  }

  /**
   * Handles sorting the studies based on the sort option selected
   * @param {Object} event The onChange event that triggers the handleSort logic
   */
  handleSort = event => {
    var sortOption = event.target.value;
    var listToSort = this.state.filteredStudies;
    this.applySort(sortOption, listToSort);
  }

  /**
   * Applies the desired sort to the list of studies
   * @param {String} sortOption The sort option that should be applied to the list of studies
   * @param {Map} listToSort The of studies to sort
   */
  applySort(sortOption, listToSort){
    var sortedList = [];

    if(sortOption == "Recently Published"){
      sortedList = listToSort.sort((a,b) => {
        var da = new Date(a.published),
            db = new Date(b.published);
        return db - da;
      });
    }
    else if(sortOption == "Phase"){
      sortedList = listToSort.sort((a, b) => (a.phasePriority > b.phasePriority) ? 1 : -1);
    }
    else if(sortOption == "Primary Completion Date"){
      sortedList = listToSort.sort((a,b) => {
        var da = new Date(a.completionDate),
            db = new Date(b.completionDate);
        return da - db;
      });
    }

    this.setState({
      filteredStudies: sortedList,
      currentPage: 1,
      sortOption: sortOption
    });
  }

  /**
   * Determines if the list of filters and sort options should display or not
   * @param {Object} event The onClick event that triggers the filterButton logic
   */
  filterButton = event => {
    if(this.state.showFilter == "none"){
      this.setState({
        showFilter: "grid"
      });
    }
    else{
      this.setState({
        showFilter: "none"
      });
    }
  }

  /**
   * Allows users to open the "View all studies on ClinicalTrials.gov" link
   * @param {String} searchTerm The word or phrase used for retrieving search results
   */
  viewAllStudies(searchTerm) {
    searchTerm = this.cleanSearchTerm(searchTerm);
    var url = 'https://clinicaltrials.gov/search?spons=' + searchTerm + '#ResultsPageForm';
    quip.apps.openLink(url);
  }

  /**
   * Returns the studies for the current page of results
   * @return {Map} currentStudies Studies for the current page of results
   */
  getCurrentStudies() {
    const indexOfLastStudy = this.state.currentPage * this.state.studiesPerPage;
    const indexOfFirstStudy = indexOfLastStudy - this.state.studiesPerPage;
    const currentStudies = this.state.filteredStudies.slice(indexOfFirstStudy, indexOfLastStudy);
    return currentStudies;
  }

  /**
   * Allows for a smooth transition through each page of studies 
   * @param {Number} pageNumber The page number of the studies that should display
   */
  changePage = pageNumber => {
    this.setState({currentPage: pageNumber});
    this.ref.current.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
    });
  }

  /**
   * Retrieves the updated search tearm and searchs for a new batch of studies  
   */
  searchForStudies() {
    this.setState(
      {searchTerm: quip.apps.getRootRecord().get("account").getTextContent()},
      () => this.getClinicalStudies(this.state.searchTerm)
    );
  }

  render() {
    var studies = this.getCurrentStudies();
    const record = quip.apps.getRootRecord();
    const account = record.get("account");

    return (
      <div>
        <div>
          <h1 className="quip-text-h2">Clinical Studies (ClinicalTrials.gov)</h1>
        </div>

        <div className={Styles.searchDiv}>
          <div className={Styles.searchBox}>
              <quip.apps.ui.RichTextBox record={account} width="200px" />
          </div>
          <div className={Styles.searchButton} onClick={() => this.searchForStudies()}>Search</div>
        </div>

        <div>
          <div className={Styles.feedButton} onClick={this.filterButton}>
            <p>Filter + Sort</p>
          </div>
          <div className={Styles.filterContainer} style={{display: this.state.showFilter}}>
            <div className={Styles.recruStatus}>
              <h2>Recruitment Status</h2>
              {this.state.filters.map(filter => {
                if(filter.type == "status") {
                  return(<StudyFilter handleChange={this.handleFilter} {...filter} />)
                }
              })}
            </div>
            <div className={Styles.phase}>
              <h2>Phase</h2>
              {this.state.filters.map(filter => {
                if(filter.type == "phase") {
                  return(<StudyFilter handleChange={this.handleFilter} {...filter} />)
                }
              })}
            </div>
            <div>
              <h2>Primary Completion Date</h2>
              <select onChange={this.handleDateFilter}>
                <option>All</option>
                <option>Next 3 Months</option>
                <option>Next 6 Months</option>
                <option>Next 12 Months</option>
              </select>
            </div>
            <div className={Styles.sortBy}>
              <h2>Sort By</h2>
              <select onChange={this.handleSort}>
                <option>Recently Published</option>
                <option>Phase</option>
                <option>Primary Completion Date</option>
              </select>
            </div>
          </div>
        </div>

        <div ref={this.ref}>
            {
              this.state.resultsAvailable && 
              <p className={Styles.studyLink} onClick={(event) => this.viewAllStudies(this.state.searchTerm)}>View all studies on ClinicalTrials.gov</p>
            }
        </div>

        <div>
          {
            this.state.error && 
            <p>There was an error loading the studies.</p>
          }
        </div>

        <Studies studyList={studies} filteredList={this.state.filteredStudies} searchTerm={this.state.searchTerm} availble={this.state.resultsAvailable}/>
        <Pagination currentPage={this.state.currentPage} studiesPerPage={this.state.studiesPerPage} totalStudies={this.state.filteredStudies.length} changePage={this.changePage}/>
      </div>
    );
  }
}