import React from "react";
import Styles from "../App.less";

/** Creates pagination buttons and handles the pagination logic */
export default class Pagination extends React.Component {   
    
    render() {
        var lastPage = Math.ceil(this.props.totalStudies / this.props.studiesPerPage);
        if(lastPage != 0){
            var nextPage = this.props.currentPage + 1;
            var prevPage = this.props.currentPage - 1;
            return (
                <div className={Styles.pagination}>
                    {this.props.currentPage != 1 && <p className={Styles.feedButton + ' ' + Styles.prevBtn} onClick={() => this.props.changePage(prevPage)}>&laquo; Previous</p>}
                    {/* {quip.apps.getContainerWidth() > 600 && <p className={Styles.pageNumber}>Page {this.props.currentPage} of {lastPage}</p>} */}
                    {this.props.currentPage != lastPage && <p className={Styles.feedButton + ' ' + Styles.nextBtn} onClick={() => this.props.changePage(nextPage)}>Next &raquo;</p>}
                </div>
            )
        }
        return null;
    }
}