import quip from "quip";
import React from "react";
import Styles from "../App.less";

/** Displays a list of studies in card format */
export default class Studies extends React.Component {
    /**
     * Allows users to open a link to view the desired study on the ClinicalTrials.gov website
     * @param {Object} study The study whose url should be opened
     */
    openStudy(study) {
        var url = study.url;
        quip.apps.openLink(url);
    }
    
    render() {
        var studies;

        if(this.props.availble){
            studies = this.props.studyList.map((study, i) => {
                return (
                    <div className={Styles.card}>
                        <p className={Styles.studyLink} onClick={(event) => this.openStudy(study)}><b>{study.name}</b></p>
                        <p><b>Published: </b>{study.published}<br/>
                        <b>Condition: </b>{study.condition}<br/>
                        <b>Study Start: </b>{study.startDate}<br/>
                        <b>Primary Completion: </b>{study.completionDate}<br/>
                        <b>Phase: </b>{study.phase}<br/>
                        <b>Status: </b>{study.status}<br/>
                        <b>Last Modified: </b>{study.lastModified}<br/></p>
                    </div>
                )
            });
        }
        else{
            return <p>No studies returned. Please double-check spelling, adjust filter options, or try again later.</p>
        }

        return (
            <div>
                <p>{this.props.filteredList.length} Studies found for: <b>{this.props.searchTerm}</b></p>
                <div class={Styles.feedStudies}>{studies}</div>
            </div>
        );
    }
}