import React from "react";
import Styles from "../App.less";

/** Displays a list of checkbox filters */
export default class StudyFilter extends React.Component { 
    render() {
        return(
            <div className={Styles.checkbox}>
                <input type="checkbox" name="status" value={this.props.value} id={this.props.id} onClick={() => this.props.handleChange(this)}/>
                <label for={this.props.id}>{this.props.label}</label><br/>
            </div>
        )
    }
}