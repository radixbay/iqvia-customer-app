import App from "./App.jsx";

/** Stores the account name (search term) in a Quip Root Record */
class RootRecord extends quip.apps.RootRecord {
    static getProperties = () => ({
        account: quip.apps.RichTextRecord
    })

    static getDefaultProperties = () => ({
        account: { RichText_placeholderText: "Enter Account name..." }
    })
}

quip.apps.registerClass(RootRecord, "root");

/** Initializes the Clinical Trials live app */
quip.apps.initialize({
    initializationCallback: (root, params) => {
        ReactDOM.render(<App />, root);
    },
});