export var defaultFilters = [
    {id: "notYetRecru", type: "status", value: "notYetRecru", isChecked: false, label: "Not yet recruiting"},
    {id: "recruiting", type: "status", value: "recruiting", isChecked: false, label: "Recruiting"},
    {id: "enrollByInv", type: "status", value: "enrollByInv", isChecked: false, label: "Enrolling by invitation"},
    {id: "actNotRecr", type: "status", value: "actNotRecr", isChecked: false, label: "Active, not recruiting"},
    {id: "suspended", type: "status", value: "suspended", isChecked: false, label: "Suspended"},
    {id: "terminated", type: "status", value: "terminated", isChecked: false, label: "Terminated"},
    {id: "completed", type: "status", value: "completed", isChecked: false, label: "Completed"},
    {id: "withdrawn", type: "status", value: "withdrawn", isChecked: false, label: "Withdrawn"},
    {id: "unkwnStat", type: "status", value: "unkwnStat", isChecked: false, label: "Unknown status"},
    {id: "phase1", type: "phase", value: "phase1", isChecked: false, label: "Phase 1", sortPriority: 1},
    {id: "phase2", type: "phase", value: "phase2", isChecked: false, label: "Phase 2", sortPriority: 2},
    {id: "phase3", type: "phase", value: "phase3", isChecked: false, label: "Phase 3", sortPriority: 3},
    {id: "phase4", type: "phase", value: "phase4", isChecked: false, label: "Phase 4", sortPriority: 4},
    {id: "na", type: "phase", value: "na", isChecked: false, label: "Not Applicable", sortPriority: 5}
]